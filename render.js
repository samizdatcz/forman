function enableBtn() {
    document.getElementById("poslat").disabled = false;
   	}

$(document).ready(function() {
    document.getElementById("poslat").disabled = true;

    if (window.location.href.slice(-1) == "t") {
    	$("#alert1").removeClass("hidden");
    }    

    $.getJSON( "https://spreadsheets.google.com/feeds/list/1TEFJPgs9XdU9WcQQ3yMBLnrQA9JiIwJWy44_cVxP6aE/orbu3j6/public/values?alt=json", function( data ) {
      
      var vzkazy = [];
      var podpisy =[];
      $.each( data.feed.entry, function( key, val ) {
        if( val.gsx$schvaleno.$t=="1") {
          vzkazy.push( "<p>" + val.gsx$vzkaz.$t + "</p>" );
          podpisy.push( "<p><strong>– " + val.gsx$jmeno.$t + "</strong></p>" );
        }
      });
     
     for (var i = vzkazy.length - 1; i >= 0; i--) {
     	$( ".container" ).append(vzkazy[i]);
     	$( ".container" ).append(podpisy[i]);
     	$( ".container" ).append("<hr>");

     }
      
    });
});